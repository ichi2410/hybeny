<?php
  $params = $_GET;
  $a = array();
  $b = array();
    
  if(!$params['a']){
  	$params['a'] = '35.57791271, 139.46154188';
  }
  
  if(!$params['b']){
  	$params['b'] = '35.66278628,139.42274355';
  }
  
  $a['latlon'] = $params['a'];
  $tmp = explode(',',$params['a']);
  $a['lat'] = $tmp[0];
  $a['lon'] = $tmp[1];
  
  $b['latlon'] = $params['b'];
  $tmp = explode(',',$params['b']);
  $b['lat'] = $tmp[0];
  $b['lon'] = $tmp[1];
  
?>
<!DOCTYPE html>
<html lang="ja">
<head>
	<meta charset="utf-8">
	<title>距離計算検証</title>
	
	<!--[if lt IE 9]>
	<script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
	<![endif]-->
	
	<script src="//code.jquery.com/jquery-1.10.2.min.js" type="text/javascript"></script>
	<script src="//maps.google.com/maps/api/js?libraries=geometry&sensor=false" type="text/javascript"></script>
	
	<script>
	
	$(function(){
	  var from = new google.maps.LatLng(<?php  echo $a['latlon'];?>);
	  var to = new google.maps.LatLng(<?php  echo $b['latlon'];?>);
	  var distance = google.maps.geometry.spherical.computeDistanceBetween(from,to);
	  
	  distance = distance / 1000;
	  
	  console.log(distance);
	  $("span#distance").html(distance);
	});
	</script>

<?php
  
  $shop = array(
    'lat'=>$a['lat'],
    'lon'=>$a['lon']
  );
  
  $staff = array(
    'lat'=>$b['lat'],
    'lon'=>$b['lon']
  );
    
  //三平方の定理
  $pythagorean_result = sqrt( pow(($a['lat'] - $b['lat'])/0.0111, 2) + pow(($a['lon'] - $b['lon'])/0.0091, 2) );
  
  $hybeny_result = hybeny($shop, $staff);  
?>
</head>
<body>

<h1>距離計算</h1>

<form method="get" action="">
  地点A : <input type="" name="a" value="<?php  echo $a['latlon'];?>" /><br />
  地点B : <input type="" name="b" value="<?php  echo $b['latlon'];?>" />
  <input type="submit" value="測定" />
</form>

<hr />
三平方の定理 : <?php echo $pythagorean_result; ?>km<br />
ヒュベニ : <?php echo $hybeny_result; ?>km<br />
google maps API : <span id="distance"></span><br />
	
</body>
</html>

<?php
  function hybeny($shop, $staff) {
    //ヒュベニの定理(簡易) / 世界測地系(yahooジオコーダはWGS)
    $shop['lat_rad'] = deg2rad($shop['lat']);
    $shop['lon_rad'] = deg2rad($shop['lon']);
    
    $staff['lat_rad'] = deg2rad($staff['lat']);
    $staff['lon_rad'] = deg2rad($staff['lon']);
    
    //平均緯度
    $P = ($shop['lat_rad']+$staff['lat_rad'])/2;
    
    //緯度差
    $dP = $staff['lat_rad'] - $shop['lat_rad'];
    
    //経度差
    $dR = $staff['lon_rad'] - $shop['lon_rad'];
    
    //子午線曲率半径を計算
    $tmp = 1-0.006694*pow(sin($P),2); //日本測地系 : 0.006674, WGS : 0.006694
    
    $M = 6335439 / sqrt(pow($tmp,3));//日本測地系 : 6334834, WGS : 6335439
    
    //卯酉線曲率半径を取得
    $N = 6378137 / sqrt($tmp);//日本測地系 : 6377397,  WGS : 6378137
    
    //ヒュベニの距離計算式
    $tmp1 = $M*$dP;
    $tmp2 = $N*cos($P)*$dR;
    
    $hybeny_result = sqrt( pow($tmp1,2) + pow($tmp2,2) );
    $hybeny_result = $hybeny_result / 1000;
    
    return $hybeny_result;
  }
?>
